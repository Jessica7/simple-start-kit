import webpack from 'webpack';
import { resolve }  from 'path';
import browserSyncPlugin from 'browser-sync-webpack-plugin';

export default {
 entry: [
  'webpack-hot-middleware/client',
  './src/index.js'
 ],
 output: {
  path: resolve(__dirname, "build"),
  filename: 'bundle.js'
},
 module: {
   loaders: [{ test: /\.(js|jsx)?$/, loader:'babel-loader', exclude: /node_modules/ }]
 },
 plugins: [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new browserSyncPlugin({
      host: 'localhost',
      port: 3000,
      proxy: 'http://localhost:4000'
    })
  ]
};
